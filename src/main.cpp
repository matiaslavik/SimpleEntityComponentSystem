#include <iostream>
#include "entity_component_system.h"

using namespace ecs;

class MyComponent : public EntityComponent
{
public:
    int mA;
    float mB;

    MyComponent(int a, float b)
    {
        mA = a;
        mB = b;
    }
};

int main()
{
    EntityComponentSystem ecs;

    Entity entity1 = ecs.createEntity();
    ecs.addComponent(entity1, MyComponent(1, 1.14f));

    Entity entity2 = ecs.createEntity();
    ecs.addComponent(entity2, MyComponent(2, 2.14f));

    Entity entity3 = ecs.createEntity();
    ecs.addComponent(entity3, MyComponent(3, 3.14f));

    ecs.removeComponent<MyComponent>(entity2);

    Entity entity4 = ecs.createEntity();
    ecs.addComponent(entity4, MyComponent(4, 4.14f));

    ecs.destroyEntity(entity3);

    Entity entity5 = ecs.createEntity();
    ecs.addComponent(entity5, MyComponent(5, 5.14f));

    const std::vector<MyComponent> comps = ecs.getComponents<MyComponent>();

    bool entity1HasComp = ecs.hasComponent<MyComponent>(entity1);
    bool entity2HasComp = ecs.hasComponent<MyComponent>(entity2);
    MyComponent entity4Comp = ecs.getComponent<MyComponent>(entity4);

    return 0;
}
