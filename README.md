# SimpleEntityComponentSystem

This is a naive implementation of an Entity Component System (made for educational purposes, and as an example).

It allows you to store entities and components linearly in memory, and systems that need to iterate over all components of a certain type can do so in a cache friendly manner.

However, the system has a few flaws:
- There is a hard limit on the maximum number of entities.
- There is no cache-friendly way of iterating over all components of all entities. If a renderer needs to iterate though all Entities and access their RenderData and Transform components, then it will need to call getComponent<T>() twice on each entry.
- If you have (large) components that only a few entities use, then you will waste a lot of memory. This is because we have to allocate enough memory for *all* entities having *all* components.

A better solution would be to group together entities of the same "archetype" and their components. This seems to be what Unity does.
