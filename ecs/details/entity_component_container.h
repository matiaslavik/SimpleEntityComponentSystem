#pragma once
#include "ecs_types.h"

namespace ecs
{
namespace details
{
    class EntityComponentContainerBase
    {
    public:
        virtual ~EntityComponentContainerBase() = default;
        virtual void removeComponent(Entity entity) = 0;
        virtual bool containsEntity(Entity entity) = 0;
    };

    template<typename T, int EntityCount>
    class EntityComponentContainer : public EntityComponentContainerBase
    {
    private:
        std::vector<T> mComponents;
        std::array<size_t, EntityCount> mEntityToComponentMap;
        std::array<Entity, EntityCount> mComponentToEntityMap;

    public:
        ~EntityComponentContainer() {}

        EntityComponentContainer()
        {
            mComponents.reserve(EntityCount);
            for (int i = 0; i < EntityCount; ++i)
                mEntityToComponentMap[i] = -1;
        }

        void addComponent(Entity entity, T component)
        {
            size_t index = mComponents.size();
            mComponents.push_back(component);
            mEntityToComponentMap[entity] = index;
            mComponentToEntityMap[index] = entity;
        }

        void removeComponent(Entity entity)
        {
            size_t compIndex = mEntityToComponentMap[entity];
            size_t tailIndex = mComponents.size() - 1;
            Entity tailEntity = mComponentToEntityMap[tailIndex];

            T tailComp = mComponents[tailIndex];
            mComponents[compIndex] = tailComp;
            mEntityToComponentMap[tailEntity] = compIndex;
            mEntityToComponentMap[entity] = -1;
            mComponentToEntityMap[compIndex] = tailEntity;

            mComponents.pop_back();
        }

        bool containsEntity(Entity entity) override
        {
            return mEntityToComponentMap[entity] != -1;
        }

        T getComponent(Entity entity)
        {
            size_t compIndex = mEntityToComponentMap[entity];
            assert(compIndex != -1);
            return mComponents[compIndex];
        }

        const std::vector<T> getComponents()
        {
            return mComponents;
        }
    };
}
}
