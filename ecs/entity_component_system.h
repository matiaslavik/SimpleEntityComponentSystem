#pragma once
#include <vector>
#include <array>
#include <cassert>
#include <type_traits>
#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include "ecs_types.h"
#include "details/entity_component_container.h"

namespace ecs
{
    class EntityComponentSystem
    {
    private:

        static const int MAX_ENTITY_COUNT = 10000;

        std::vector<Entity> mAvailableEntities;

        std::unordered_map<std::type_index, std::unique_ptr<details::EntityComponentContainerBase>> mComponentContainers;

        template<typename T>
        auto getComponentContainer()
        {
            static_assert(std::is_base_of<EntityComponent, T>::value, "Must be derived from EntityComponent");
            
            std::type_index compTypeIndex(typeid(T));
            auto itCompConainer = mComponentContainers.find(compTypeIndex);
            if (itCompConainer == mComponentContainers.end())
            {
                mComponentContainers.emplace(std::make_pair(compTypeIndex, std::make_unique<details::EntityComponentContainer<T, MAX_ENTITY_COUNT>>()));
                itCompConainer = mComponentContainers.find(compTypeIndex);
            }
            details::EntityComponentContainer<T, MAX_ENTITY_COUNT>* compContainer = static_cast<details::EntityComponentContainer<T, MAX_ENTITY_COUNT>*>(itCompConainer->second.get());
            return compContainer;
        }

    public:
        EntityComponentSystem()
        {
            mAvailableEntities.resize(MAX_ENTITY_COUNT);
            for (int i = 0; i < MAX_ENTITY_COUNT; i++)
            {
                Entity entity = static_cast<Entity>(i);
                mAvailableEntities[i] = entity;
            }
        }

        Entity createEntity()
        {
            assert(mAvailableEntities.size() > 0);

            Entity entity = mAvailableEntities.back();
            mAvailableEntities.pop_back();
            return entity;
        }

        void destroyEntity(Entity entity)
        {
            for (auto& compContainer : mComponentContainers)
            {
                if (compContainer.second->containsEntity(entity))
                    compContainer.second->removeComponent(entity);
            }

            mAvailableEntities.push_back(entity);
        }

        template<typename T>
        void addComponent(Entity entity, T component)
        {
            auto compContainer = getComponentContainer<T>();
            compContainer->addComponent(entity, component);
        }

        template<typename T>
        void removeComponent(Entity entity)
        {
            auto compContainer = getComponentContainer<T>();
            compContainer->removeComponent(entity);
        }

        template<typename T>
        bool hasComponent(Entity entity)
        {
            auto compContainer = getComponentContainer<T>();
            return compContainer->containsEntity(entity);
        }

        template<typename T>
        T getComponent(Entity entity)
        {
            auto compContainer = getComponentContainer<T>();
            return compContainer->getComponent(entity);
        }

        template<typename T>
        const std::vector<T> getComponents()
        {
            auto compContainer = getComponentContainer<T>();
            return compContainer->getComponents();
        }

    };
}
